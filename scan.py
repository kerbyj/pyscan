#!/usr/bin/python
# -*- coding: UTF-8 -*-

import sys
import argparse
from bs4 import BeautifulSoup
import urllib2
import jsbeautifier
import time

global namespace;
global vulners;
vulners=['eval(', "innerHTML", "XMLHttpRequest"]


def find_param():
    parser = argparse.ArgumentParser()
    parser.add_argument ('-u', '--url', default=True)
    parser.add_argument ('-d', '--delay', default=0)
    return parser

def parse_bs(src_code):

    full_list=[]
    src=BeautifulSoup(src_code, 'lxml')
    print("----- Scanning -----\n")
    linked_js=[script['src'] for script in src.find_all('script', src=True)]
    builtin_js=[str(script) for script in src.find_all('script', src=False)]
    for js in linked_js:
        res=check_url(namespace.url, js)
        if res==1:
            full_list.append(namespace.url+js)
        if res==2:
            full_list.append(js)
        if res==3:
            full_list.append(js.replace("//","https://"))

    print("----- Scan for unsafe function -----\n")

    for link in full_list:

        print("\x1b[37;45mObject: "+link+"\x1b[0m")
        js_src=jsbeautifier.beautify(urllib2.urlopen(link).read()).split("\n")
        print("Here are {} rows".format(len(js_src)))
        for i in range(len(js_src)):
            control_string=js_src[i-1].replace(" ","")
            if len(control_string)>150: continue;
            for vulner in vulners:

                if vulner in control_string:
                    current_out=range(i-3, i+3, 1)
                    for k in range(len(current_out)-1):

                        if(current_out[k]>=0):
                            if len(js_src[current_out[k]])>150:
                                print("String {} ".format(current_out[k])+"too long")
                                continue
                            if(current_out[k]==i-1):
                                print("\x1b[31mString {}:\x1b[0m ".format(current_out[k])+js_src[current_out[k]])
                            else:
                                print("String {}: ".format(current_out[k])+js_src[current_out[k]])
                    print("----------------\n")



    print("\x1b[37;45mIn src code:\x1b[0m")
    for script in builtin_js:

        js_src=jsbeautifier.beautify(script).split("\n")
        print("Here are {} rows".format(len(js_src)))
        for i in range(len(js_src)):
            control_string=js_src[i-1].replace(" ","")
            if len(control_string)>150: continue;
            for vulner in vulners:

                if vulner in control_string:
                    current_out=range(i-3, i+3, 1)
                    for k in range(len(current_out)-1):

                        if(current_out[k]>=0):
                            if len(js_src[current_out[k]])>150:
                                print("String {} ".format(current_out[k])+"too long")
                                continue
                            if(current_out[k]==i-1):
                                print("\x1b[31mString {}:\x1b[0m ".format(current_out[k])+js_src[current_out[k]])
                            else:
                                print("String {}: ".format(current_out[k])+js_src[current_out[k]])
                    print("----------------\n")

#temporary solution
def check_url(url, js):
    try:
        urllib2.urlopen(url+js)
        return 1
    except:
        try:
            urllib2.urlopen(js)
            return 2
        except:
            try:
                urllib2.urlopen(js.replace("//","https://"))
                return 3
            except:
                return 0

if __name__ == '__main__':
    parser = find_param()
    namespace = parser.parse_args(sys.argv[1:])
    print ("----- Target: {} -----\n".format(namespace.url))
    src_code=urllib2.urlopen(namespace.url)
    parse_bs(src_code.read())
    print(src_code.read())
