from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
import os
import time
import re
import mysql.connector as mariadb

if __name__ == '__main__':
    driver = webdriver.Chrome('/home/kerby/projects/bak/chromedriver')
    driver.set_window_size(1920, 1080)
    mariadb_connection = mariadb.connect(user='user', password='pass', database='work')
    data = mariadb_connection.cursor()
    malware_list=["toolkit", "remote", "rat", "spyware", "networkworm", "worm", "honeypot", "dropper", "backdoor", "bot", "iot", "malware",
    "trojan", "network", "ssh", "ftp", "rootkit", "banker", "ransom", "ransomware", "ethernalblue",
    "exploit", "cve", "linux", "windows", "android", "mirai"]
    for malware in malware_list:
        driver.get("https://www.virustotal.com/ru/")
        driver.find_element_by_id("search-tab-chooser").click()
        driver.find_element_by_id("query").send_keys(malware)
        driver.find_element_by_id("search").click()
        time.sleep(2.5)
        try:
            for i in range(3):
                driver.find_element_by_id("btn-more-comments").click()
                time.sleep(2.5)
        except Exception:
            print("No more")
        src_code=src=BeautifulSoup(driver.page_source, 'lxml')
        all_files=[a['href'] for a in src_code.find_all('a', href=re.compile(r'^/ru/file/\w*'))]
        data.execute("SELECT link FROM malwares")
        already=[link[0] for link in data]
        count=0
        for element in all_files:
            if element in already:
                continue
            # print("Element added: %s", (element))
            count+=1
            data.execute("INSERT INTO malwares (type, link) VALUES (%s, %s)", (malware, element))
        mariadb_connection.commit()
        print("{} added from the {} ({}/{})".format(str(count), malware, str(malware_list.index(malware)+1), str(len(malware_list))))
    driver.close()
    os.system("killall chromedriver")
